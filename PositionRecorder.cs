﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionRecorder : MonoBehaviour
{
    public List<KeyframeData> keyframeData;
    public bool record = true;
    public bool reverse;

    public int currentFrame { get; private set; }

    private void Start()
    {
        keyframeData = new List<KeyframeData>();

        Manager.RegisterRecordEvent(Record);
        Manager.RegisterReverseEvent(Reverse);
        Manager.RegisterRestartEvent(Restart);
    }

    private void FixedUpdate()
    {
        currentFrame = keyframeData.Count - 1;
    }

    void Record()
    {
        if(record)
        {
            keyframeData.Add(new KeyframeData(transform.localPosition, transform.localRotation));
        }

    }

    void Reverse(int speed)
    {
        if (reverse)
        {
            if (keyframeData.Count >= 1)
            {
                transform.localPosition = keyframeData[keyframeData.Count - 1].position;
                transform.localRotation = keyframeData[keyframeData.Count - 1].rotation;
                keyframeData.RemoveRange(keyframeData.Count - speed, speed);

            }
            else
            {
                Record();
            }
        }
    }

    void Restart()
    {
        keyframeData.Clear();
    }

    private void OnDestroy()
    {
        Manager.UnregisterRecordEvent(Record);
        Manager.UnregisterReverseEvent(Reverse);
        Manager.UnregisterRestartEvent(Restart);
    }
}
